interface IResultResponse {
    success: boolean;
    errorMessage: string | null
}

export interface ISearchResultResponse extends IResultResponse {
    data: Array<IMovieShortDto>;
    total: number;
}

export interface IMovieDetailResultResponse extends IResultResponse {
    data: IMovieDetailDto;
    success: boolean;
    errorMessage: string | null;
}

export interface IMovieShortDto {
    imdbID: string;
    title: string;
    year: number;
}

export interface IMovieDetailDto extends IMovieShortDto {
    timeLength: string | null;
    genre: string | null;
    writer: string | null;
    actors: string | null;
    language: string | null;
    country: string | null;
    posterUrl: string | null;
}