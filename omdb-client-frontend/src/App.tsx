import { Route, RouterProvider, createBrowserRouter, createRoutesFromElements, Outlet } from "react-router-dom";

import { MovieDetails } from "./components/MovieDetails/MovieDetails";
import { SearchHistory } from "./components/SearchHistory/SearchHistory";
import { Search } from "./components/Search/Search";
import { SearchResults } from "./components/SearchResults/SearchResults";

const router = createBrowserRouter(
    createRoutesFromElements(
        <Route path="/" element={<Root />}>
            <Route index element={<Index />} />
            <Route path="movie/:id" element={<MovieDetails />} />
            <Route path="*" element={<Error />} />
        </Route>
    )
);

function Root() {
    return (
        <>
            <Search />
            <Outlet />
        </>
    )
}

function Index() {
    return (
        <>
            <SearchHistory />
            <SearchResults />
        </>
    )
}

function Error() {
    return (
        <b>Error page</b>
    )
}

export default function App() {
    return (
        <RouterProvider router={router}></RouterProvider>
    )
};