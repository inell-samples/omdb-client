import { useSelector } from "react-redux";
import { RootState } from "../../store/store";

import style from './SearchHistory.module.less'

export function SearchHistory() {

    const items = useSelector((state: RootState) => state.queryHistory);

    return (
        <>
            {items.map((item: string) => (<span key={item} className={style.historyItem}>{item}</span>))}
        </>
    )
}