import { FormEvent } from "react";

import style from "./Search.module.less";

import { AppDispatch, useAppDispatch } from "../../store/store";
import { submitSearchForm } from "../../store/movieSearchSlice";
import { NavigateFunction, useNavigate } from "react-router-dom";
import { addSearchHistoryItem } from "../../store/searchHistorySlice";

export function Search() {

    const dispatch = useAppDispatch();
    const navigate = useNavigate();

    return (
        <form method="post" className={style.form} onSubmit={e => onSubmit(e, dispatch, navigate)}>
            <input type="text" name="title" required placeholder="Type movie title..." className={style.input}></input>
            <button type="submit" className={style.button}>Submit</button>
        </form>
    )
}

function onSubmit(event: FormEvent<HTMLFormElement>, dispatch: AppDispatch, navigate: NavigateFunction) {

    event.preventDefault();

    const form = event.target as HTMLFormElement;
    const data = new FormData(form);

    dispatch(addSearchHistoryItem(data.get("title") ?? "AAA"))
    dispatch(submitSearchForm(data));

    navigate("/");
}