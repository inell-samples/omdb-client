import { useEffect } from "react";
import { useSelector } from "react-redux";
import { useParams } from "react-router-dom";
import { fetchMovieDetals } from "../../store/movieDetailsSlice";
import { LoadingState, RootState, useAppDispatch } from "../../store/store";
import { IMovieDetailDto } from "../../models/DataModels";

export function MovieDetails() {

    const dispatch = useAppDispatch();

    const { id } = useParams();
    const { data, status } = useSelector((state: RootState) => state.movieDetails);
    const movie = data?.data;

    useEffect(() => {
        dispatch(fetchMovieDetals(id));
    }, [id, dispatch]);

    return (
        <section>
            {
                {
                    [LoadingState.Pending]: <pre>Loading data...</pre>,
                    [LoadingState.Failed]: <pre>Failed to feth movie details: {data?.errorMessage ?? "No backend response"}</pre>,
                    [LoadingState.Succeeded]: (movie && <MovieDetailedItem movie={movie} />) ?? <pre>There is no movie with code: {id}</pre>,
                    [LoadingState.Idle]: null,
                }[status]
            }
        </section>
    )
}

function MovieDetailedItem({ movie }: { movie: IMovieDetailDto }) {

    return (
        <article>
            <h2>{movie.title} ({movie.year})</h2>
            {movie.posterUrl && <img src={movie.posterUrl!} alt={movie.title}/>}
            <p><b>Genre</b>: {movie.genre}</p>
            <p><b>Actors</b>: {movie.actors}</p>
            <p><b>Country</b>: {movie.country}</p>
            <p><b>Language</b>: {movie.language}</p>
            <p><b>Length</b>: {movie.timeLength}</p>
        </article>
    )
}