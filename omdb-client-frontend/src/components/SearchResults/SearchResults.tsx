import { useSelector } from "react-redux";

import { LoadingState, RootState } from "../../store/store";
import { IMovieShortDto } from "../../models/DataModels";
import { NavLink } from "react-router-dom";

export function SearchResults() {

    const { data, status } = useSelector((state: RootState) => state.movieSearch);
    const movies = data?.data;

    return (
        <section>
            {
                {
                    [LoadingState.Pending]: <pre>Loading data...</pre>,
                    [LoadingState.Failed]: <pre>Failed to feth search results: {data?.errorMessage ?? "No backend response"}</pre>,
                    [LoadingState.Succeeded]: <>
                        {
                            movies?.length !== 0
                                ? <ul>
                                    {movies?.map((item: IMovieShortDto) => <MovieShotItem key={item.imdbID} movie={item} />)}
                                </ul>
                                : <pre>Nothing was found</pre>
                        }
                    </>,
                    [LoadingState.Idle]: null,
                }[status]
            }
        </section>
    )
}

function MovieShotItem({ movie }: { movie: IMovieShortDto }) {
    return (
        <li>
            <NavLink to={`/movie/${movie.imdbID}`}>{movie.year} - {movie.title}</NavLink>
        </li>
    );
}