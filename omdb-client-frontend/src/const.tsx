export const API_URL = "http://localhost:5203/api/omdb";
export const SEARCH_URL = `${API_URL}/search`;
export const DETAILS_URL = `${API_URL}/details`;