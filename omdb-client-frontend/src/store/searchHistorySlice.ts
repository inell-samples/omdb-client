// https://github.com/reduxjs/redux-toolkit/issues/1960
import * as toolkitRaw from '@reduxjs/toolkit';
const { createSlice } = ((toolkitRaw as any).default ?? toolkitRaw) as typeof toolkitRaw;

const key = 'searchHistory';
const initialState = JSON.parse(localStorage.getItem(key) || '[]') as Array<string>;

const searchHistorySlice = createSlice({
    name: 'searchHistory',
    initialState,
    reducers: {
        addSearchHistoryItem(state, action) {
            
            const searchQuery = action.payload;

            if (state.includes(searchQuery)) {
                return;
            }

            state.push(searchQuery);

            // Save latest 5 items in query history
            if (state.length > 5) {
                const tail = state.slice(1, 6);
                state.splice(0, state.length);
                state.push(...tail);
            }

            localStorage.setItem(key, JSON.stringify(state));
        }
    }
});

export const { addSearchHistoryItem } = searchHistorySlice.actions;

export default searchHistorySlice.reducer;