// Import reduxjs such way because it is not yet Node-ESM compatible
// https://github.com/reduxjs/redux-toolkit/issues/1960
import * as toolkitRaw from '@reduxjs/toolkit';
const { createSlice, createAsyncThunk } = ((toolkitRaw as any).default ?? toolkitRaw) as typeof toolkitRaw;

import { LoadingState } from './store';
import { SEARCH_URL } from "../const";
import { ISearchResultResponse } from '../models/DataModels';

export const submitSearchForm = createAsyncThunk('movieSearch/submitSearch', async (data: FormData) => {

    console.dir(...data);
    
    const url = new URL(SEARCH_URL);
    url.searchParams.append("title", data.get("title")?.toString() ?? "");

    const responce = await fetch(url);
    return await responce.json();
});

const movieSearchSlice = createSlice({
    name: 'movieSearch',
    initialState: {
        status: 'idle' as LoadingState,
        data: {} as ISearchResultResponse | null,
        error: null as string | null | undefined,
    },
    reducers: {},
    extraReducers(builder) {
        builder
            .addCase(submitSearchForm.pending, state => {
                state.status = LoadingState.Pending;
                state.data = null;
                state.error = null;
            })
            .addCase(submitSearchForm.fulfilled, (state, action) => {
                state.data = action.payload;
                state.status = state.data?.success
                    ? LoadingState.Succeeded
                    : LoadingState.Failed;
                state.error = state.data?.errorMessage;
            })
            .addCase(submitSearchForm.rejected, (state, action) => {
                state.status = LoadingState.Failed;
                state.error = action.error.message;
            });
    }
});

export default movieSearchSlice.reducer;