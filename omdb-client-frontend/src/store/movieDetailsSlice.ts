// https://github.com/reduxjs/redux-toolkit/issues/1960
import * as toolkitRaw from '@reduxjs/toolkit';
const { createSlice, createAsyncThunk } = ((toolkitRaw as any).default ?? toolkitRaw) as typeof toolkitRaw;

import { LoadingState } from './store';
import { DETAILS_URL } from "../const";
import { IMovieDetailResultResponse } from '../models/DataModels';

export const fetchMovieDetals = createAsyncThunk('movieDetails/fetch', async (imdbID: string | undefined) => {

    const url = new URL(DETAILS_URL);
    url.searchParams.append("imdbID", imdbID ?? "");

    const responce = await fetch(url);
    return await responce.json();
});

const movieDetailsSlice = createSlice({
    name: 'movieDetails',
    initialState: {
        status: 'idle' as LoadingState,
        data: {} as IMovieDetailResultResponse | null,
        error: null as string | null | undefined,
    },
    reducers: {},
    extraReducers(builder) {
        builder
            .addCase(fetchMovieDetals.pending, state => {
                state.status = LoadingState.Pending;
                state.data = null;
                state.error = null;
            })
            .addCase(fetchMovieDetals.fulfilled, (state, action) => {
                state.data = action.payload;
                state.status = state.data?.success
                    ? LoadingState.Succeeded
                    : LoadingState.Failed;
                state.error = state.data?.errorMessage;
            })
            .addCase(fetchMovieDetals.rejected, (state, action) => {
                state.status = LoadingState.Failed;
                state.error = action.error.message;
            });
    }
});

export default movieDetailsSlice.reducer;