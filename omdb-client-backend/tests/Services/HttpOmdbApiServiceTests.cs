﻿using AutoFixture;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Moq;
using Moq.Protected;
using OBMDbClient.Configuration;
using OBMDbClient.Models.OmdbApi;
using System.Collections.Specialized;
using System.Net;
using System.Text.Json;
using System.Web;
using Xunit;

#pragma warning disable CS8625 // Cannot convert null literal to non-nullable reference type.
namespace OBMDbClient.Services
{
    public class HttpOmdbApiServiceTests
    {
        private readonly Mock<DelegatingHandler> _httpHandler = new();
        private readonly ServicesOptions _serviceOptions;
        private readonly Mock<ILogger<HttpOmdbApiService>> _logger = new();

        private readonly Fixture _fixture = new();
        private readonly IHttpOmdbApiService _target;

        public HttpOmdbApiServiceTests()
        {
            var client = new HttpClient(_httpHandler.Object);

            _serviceOptions = _fixture.Create<ServicesOptions>();
            var options = new Mock<IOptions<ServicesOptions>>();
            options.Setup(x => x.Value).Returns(_serviceOptions);

            _target = new HttpOmdbApiService(client, options.Object, _logger.Object);
        }

        [Fact]
        public async Task GetMovieDetails_ArgumentNull_Throws()
        {
            await Assert.ThrowsAsync<ArgumentNullException>(() => _target.GetMovieDetails(null));
        }

        [Fact]
        public async Task Search_ArgumentNull_Throws()
        {
            await Assert.ThrowsAsync<ArgumentNullException>(() => _target.Search(null));
        }

        [Fact]
        public async Task GetMovieDetails_CorrectRequest_ReturnsResult()
        {
            // Arrange 
            Uri? uriCallback = null;
            SetupHttpResponse(_httpHandler, new HttpResponseMessage()
            {
                StatusCode = HttpStatusCode.OK,
                Content = new StringContent(JsonSerializer.Serialize(_fixture.Create<MovieResponse>()))
            })
                .Callback((HttpRequestMessage request, CancellationToken _) =>
                {
                    uriCallback = request.RequestUri;
                });

            var id = _fixture.Create<string>();

            // Act
            var result = await _target.GetMovieDetails(id);

            // Assert
            Assert.NotNull(result);

            var query = HttpUtility.ParseQueryString(uriCallback?.Query ?? string.Empty);

            AssertRequestArgument(query, "apikey", _serviceOptions.OmdbApiKey);
            AssertRequestArgument(query, "r", "json");
            AssertRequestArgument(query, "i", id);
        }

        [Fact]
        public async Task GetMovieDetails_IncorrectResponse_ReturnsDefault()
        {
            // Arrange
            SetupHttpResponse(_httpHandler, new HttpResponseMessage()
            {
                StatusCode = HttpStatusCode.OK,
                Content = new StringContent(_fixture.Create<string>())
            });

            // Act
            var result = await _target.GetMovieDetails(_fixture.Create<string>());

            // Assert
            Assert.Null(result);
            this.LoggerVerify(LogLevel.Warning);
        }

        [Fact]
        public async Task Search_CorrectRequest_ReturnsResult()
        {
            // Arrange 
            Uri? uriCallback = null;
            SetupHttpResponse(_httpHandler, new HttpResponseMessage()
            {
                StatusCode = HttpStatusCode.OK,
                Content = new StringContent(JsonSerializer.Serialize(_fixture.Create<SearchResponse>()))
            })
                .Callback((HttpRequestMessage request, CancellationToken _) =>
                {
                    uriCallback = request.RequestUri;
                });

            var title = _fixture.Create<string>();

            // Act
            var result = await _target.Search(title);

            // Assert
            Assert.NotNull(result);

            var query = HttpUtility.ParseQueryString(uriCallback?.Query ?? string.Empty);
            AssertRequestArgument(query, "apikey", _serviceOptions.OmdbApiKey);
            AssertRequestArgument(query, "r", "json");
            AssertRequestArgument(query, "s", title);
        }

        [Fact]
        public async Task Search_IncorrectResponse_ReturnsDefault()
        {
            // Arrange
            SetupHttpResponse(_httpHandler, new HttpResponseMessage()
            {
                StatusCode = HttpStatusCode.OK,
                Content = new StringContent(_fixture.Create<string>())
            });

            // Act
            var result = await _target.Search(_fixture.Create<string>());

            // Assert
            Assert.Null(result);
            this.LoggerVerify(LogLevel.Warning);
        }

        private static Moq.Language.Flow.IReturnsResult<DelegatingHandler> SetupHttpResponse(Mock<DelegatingHandler> httpHandler, HttpResponseMessage responseMessage)
        {
            // Mock of SendAsync() method in DelegatingHandler provided to HttpClient allows overriding client's behavior
            return httpHandler.Protected()
                .Setup<Task<HttpResponseMessage>>("SendAsync", ItExpr.IsAny<HttpRequestMessage>(), ItExpr.IsAny<CancellationToken>())
                .ReturnsAsync(responseMessage);
        }

        private static void AssertRequestArgument(NameValueCollection query, string argumentName, string expectedValue)
        {
            Assert.Contains(argumentName, query.Keys.Cast<string>());
            Assert.Equal(expectedValue, query[argumentName]);
        }

        private void LoggerVerify(LogLevel logLevel)
        {
            _logger.Verify(
                x => x.Log(
                    It.Is<LogLevel>(level => level == logLevel),
                    It.IsAny<EventId>(),
                    It.IsAny<It.IsAnyType>(),
                    It.IsAny<Exception>(),
                    It.IsAny<Func<It.IsAnyType, Exception?, string>>()));
        }
    }
}
#pragma warning restore CS8625 // Cannot convert null literal to non-nullable reference type.