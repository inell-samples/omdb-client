﻿using AutoFixture;
using Moq;
using OBMDbClient.Models.OmdbApi;
using Xunit;

#pragma warning disable CS8625 // Cannot convert null literal to non-nullable reference type.
namespace OBMDbClient.Services
{
    public class OmdbClientServiceTests
    {
        private readonly Mock<IHttpOmdbApiService> _apiService = new();

        private readonly Fixture _fixture = new();
        private readonly IOmdbClientService _target;

        public OmdbClientServiceTests()
        {
            _target = new OmdbClientService(_apiService.Object);
        }

        [Fact]
        public async Task GetMovieDetails_ArgumentNull_Throws()
        {
            await Assert.ThrowsAsync<ArgumentNullException>(() => _target.GetMovieDetails(null));
        }

        [Fact]
        public async Task Search_ArgumentNull_Throws()
        {
            await Assert.ThrowsAsync<ArgumentNullException>(() => _target.Search(null));
        }

        [Fact]
        public async Task Search_MovieNotFound_ReturnsResult()
        {
            // Arrange
            var searchResult = _fixture.Build<SearchResponse>()
                .Without(x => x.Search)
                .Without(x => x.TotalResults)
                .With(x => x.Response, false)
                .With(x => x.Error, "Movie not found!")
                .Create();

            _apiService.Setup(x => x.Search(It.IsAny<string>()))
                .ReturnsAsync(searchResult);

            // Act
            var result = await _target.Search(_fixture.Create<string>());

            // Assert
            Assert.Empty(result.Data);
            Assert.Equal(0, result.Total);
            Assert.True(result.Success);
            Assert.Null(result.ErrorMessage);
        }

        [Fact]
        public async Task Search_NullResponse_ReturnsResult()
        {
            // Arrange
            _apiService.Setup(x => x.Search(It.IsAny<string>()))
                .Returns(Task.FromResult<SearchResponse?>(null));

            // Act
            var result = await _target.Search(_fixture.Create<string>());

            // Assert
            Assert.Empty(result.Data);
            Assert.False(result.Success);
            Assert.NotNull(result.ErrorMessage);
        }

        [Theory]
        [InlineData("2023")]
        [InlineData("2022-2023")]
        public async Task Search_HasResponse_ReturnsResult(string year)
        {
            // Arrange
            var movie = _fixture.Build<MovieShort>()
                .With(x => x.Year, year)
                .Create();

            var searchResult = _fixture.Build<SearchResponse>()
                .With(x => x.Search, new[] { movie })
                .With(x => x.Response, true)
                .With(x => x.TotalResults, 1)
                .Without(x => x.Error)
                .Create();

            _apiService.Setup(x => x.Search(It.IsAny<string>()))
                .ReturnsAsync(searchResult);

            // Act
            var result = await _target.Search(movie.Title);

            // Assert
            Assert.Equal(1, result.Total);
            Assert.Equal(result.Total, result.Data.Count());
            Assert.True(result.Success);
            Assert.Null(result.ErrorMessage);
        }

        [Fact]
        public async Task Search_DataConversionError_ReturnsResult()
        {
            // Arrange
            var movie = _fixture.Build<MovieShort>()
                .With(x => x.Year, "ABCD") // Incorrect year value
                .Create();

            var searchResult = _fixture.Build<SearchResponse>()
                .With(x => x.Search, new[] { movie })
                .With(x => x.Response, true)
                .With(x => x.TotalResults, 1)
                .Without(x => x.Error)
                .Create();

            _apiService.Setup(x => x.Search(It.IsAny<string>()))
                .ReturnsAsync(searchResult);

            // Act
            var result = await _target.Search(movie.Title);

            // Assert
            Assert.Empty(result.Data);
            Assert.False(result.Success);
            Assert.NotNull(result.ErrorMessage);
        }

        [Fact]
        public async Task GetMovieDetails_IncorrectId_ReturnsResult()
        {
            // Arrange
            var movieResult = _fixture.Build<MovieResponse>()
                .Without(x => x.Year)
                .With(x => x.Response, false)
                .With(x => x.Error, "Incorrect IMDb ID.")
                .Create();

            _apiService.Setup(x => x.GetMovieDetails(It.IsAny<string>()))
                .ReturnsAsync(movieResult);

            // Act
            var result = await _target.GetMovieDetails(_fixture.Create<string>());

            // Assert
            Assert.Null(result.Data);
            Assert.True(result.Success);
            Assert.Null(result.ErrorMessage);
        }

        [Fact]
        public async Task GetMovieDetails_NullResponse_ReturnsResult()
        {
            // Arrange
            _apiService.Setup(x => x.GetMovieDetails(It.IsAny<string>()))
                .Returns(Task.FromResult<MovieResponse?>(null));

            // Act
            var result = await _target.GetMovieDetails(_fixture.Create<string>());

            // Assert
            Assert.Null(result.Data);
            Assert.False(result.Success);
            Assert.NotNull(result.ErrorMessage);
        }

        [Theory]
        [InlineData("2023")]
        [InlineData("2022-2023")]
        public async Task GetMovieDetails_HasResponse_ReturnsResult(string year)
        {
            // Arrange
            var movieResult = _fixture.Build<MovieResponse>()
                .With(x => x.Year, year)
                .With(x => x.Response, true)
                .Without(x => x.Error)
                .Create();

            _apiService.Setup(x => x.GetMovieDetails(It.IsAny<string>()))
                .ReturnsAsync(movieResult);

            // Act
            var result = await _target.GetMovieDetails(_fixture.Create<string>());

            // Assert
            Assert.NotNull(result.Data);
            Assert.True(result.Success);
            Assert.Null(result.ErrorMessage);
        }

        [Fact]
        public async Task GetMovieDetails_DataConversionError_ReturnsResult()
        {
            // Arrange
            var movieResult = _fixture.Build<MovieResponse>()
                .With(x => x.Year, "ABCD") // Incorrect year value
                .With(x => x.Response, true)
                .Without(x => x.Error)
                .Create();

            _apiService.Setup(x => x.GetMovieDetails(It.IsAny<string>()))
                .ReturnsAsync(movieResult);

            // Act
            var result = await _target.GetMovieDetails(_fixture.Create<string>());

            // Assert
            Assert.Null(result.Data);
            Assert.False(result.Success);
            Assert.NotNull(result.ErrorMessage);
        }
    }
}
#pragma warning restore CS8625 // Cannot convert null literal to non-nullable reference type.