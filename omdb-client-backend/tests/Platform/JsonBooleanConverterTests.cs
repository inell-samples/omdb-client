﻿using System.Text;
using System.Text.Json;
using Xunit;

namespace OBMDbClient.Platform
{
    public class JsonBooleanConverterTests
    {
        private readonly JsonSerializerOptions _options = new JsonSerializerOptions(JsonSerializerDefaults.Web);

        private readonly JsonBooleanConverter _target;

        public JsonBooleanConverterTests()
        {
            _target = new JsonBooleanConverter();
        }

        [Theory]
        [InlineData("False", false)]
        [InlineData("false", false)]
        [InlineData("True", true)]
        [InlineData("true", true)]
        public void Read_StringLiteral_ReturnsExpected(string value, bool expected)
        {
            // Arrange
            var json = JsonSerializer.Serialize(new
            {
                BooleanProperty = value
            });
            var reader = new Utf8JsonReader(Encoding.UTF8.GetBytes(json));

            SeekTo(ref reader, JsonTokenType.String);

            // Act
            var result = _target.Read(ref reader, typeof(bool), _options);

            // Assert
            Assert.Equal(expected, result);
        }

        [Theory]
        [InlineData(false, JsonTokenType.False, false)]        
        [InlineData(true, JsonTokenType.True, true)]
        public void Read_BooleanLiteral_ReturnsExpected(bool value, JsonTokenType tokenType, bool expected)
        {
            // Arrange
            var json = JsonSerializer.Serialize(new
            {
                BooleanProperty = value
            });
            var reader = new Utf8JsonReader(Encoding.UTF8.GetBytes(json));

            SeekTo(ref reader, tokenType);

            // Act
            var result = _target.Read(ref reader, typeof(bool), _options);

            // Assert
            Assert.Equal(expected, result);
        }

        [Fact]
        public void Read_UnconvertableStringLiteral_Throws()
        {
            // Arrange
            var json = JsonSerializer.Serialize(new
            {
                BooleanProperty = string.Empty
            });
            var reader = new Utf8JsonReader(Encoding.UTF8.GetBytes(json));

            SeekTo(ref reader, JsonTokenType.String);

            // Act
            // Can't pass ref struct to Assert.Throws() lambda, that's why such workaround
            try
            {
                _target.Read(ref reader, typeof(bool), _options);
            }
            catch (Exception ex) when (ex is not FormatException)
            {
                // Assert
                Assert.Fail("Wrong exception");
            }
            catch
            {
                // Passed
            }
        }

        /// <summary>
        /// Moves <paramref name="reader"/> to the position when meet <paramref name="tokenType"/>
        /// </summary>
        /// <param name="reader"></param>
        /// <param name="tokenType"></param>
        private static void SeekTo(ref Utf8JsonReader reader, JsonTokenType tokenType)
        {
            while (reader.TokenType != tokenType)
                if (!reader.Read())
                    break;
        }
    }
}
