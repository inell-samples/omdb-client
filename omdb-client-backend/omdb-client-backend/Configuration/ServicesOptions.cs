﻿using System.ComponentModel.DataAnnotations;

namespace OBMDbClient.Configuration;

public record ServicesOptions
{
    [Required]
    public required Uri OmdbApi { get; set; }

    [Required]
    public required string OmdbApiKey { get; set; }
}