﻿using System.Text.Json.Serialization;
using System.Text.Json;

namespace OBMDbClient.Platform;

/// <summary>
/// Provides conversion for boolean from default JSON values and from string "True"/"False"
/// </summary>
public class JsonBooleanConverter : JsonConverter<bool>
{
    public override bool Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options)
    {
        return (reader.TokenType) switch
        {
            JsonTokenType.True => true,
            JsonTokenType.False => false,
            JsonTokenType.String => bool.Parse(reader.GetString() ?? false.ToString()),
            _ => throw new JsonException()
        };
    }

    public override void Write(Utf8JsonWriter writer, bool value, JsonSerializerOptions options)
    {
        writer.WriteBooleanValue(value);
    }
}
