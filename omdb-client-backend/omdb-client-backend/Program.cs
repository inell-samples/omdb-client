using OBMDbClient.Configuration;
using OBMDbClient.Services;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
{
    var services = builder.Services;
    var config = builder.Configuration;

    services.AddCors();

    services.AddOptions<ServicesOptions>()
        .Bind(config.GetSection("Services"))
        .ValidateDataAnnotations()
        .ValidateOnStart();

    services.AddControllers();

    services.AddScoped<IOmdbClientService, OmdbClientService>();
    services.AddHttpClient<IHttpOmdbApiService, HttpOmdbApiService>();
}

var app = builder.Build();

// Configure the HTTP request pipeline.
{
    app.UseCors(options =>
    {
        options.AllowAnyOrigin();
    });

    app.MapControllers();
}

app.Run();