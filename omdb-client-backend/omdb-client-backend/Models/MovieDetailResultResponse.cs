﻿using OBMDbClient.Models.Dto;

namespace OBMDbClient.Models;

public record MovieDetailResultResponse : ResultResponse
{
    public MovieDetailDto? Data { get; set; }
}
