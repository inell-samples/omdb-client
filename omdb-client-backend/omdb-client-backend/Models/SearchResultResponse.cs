﻿using OBMDbClient.Models.Dto;

namespace OBMDbClient.Models;

public record SearchResultResponse : ResultResponse
{
    public IEnumerable<MovieShortDto> Data { get; set; } = Array.Empty<MovieShortDto>();
    public int Total { get; set; } = 0;
}
