﻿using OBMDbClient.Models.OmdbApi;
using System.Text.RegularExpressions;

namespace OBMDbClient.Models.Dto;

public record MovieShortDto
{
    public string ImdbID { get; set; }
    public string Title { get; set; }
    public int Year { get; set; }

    public MovieShortDto(MovieShort movieShort)
    {
        this.ImdbID = movieShort.ImdbID;
        this.Title = movieShort.Title;

        // Complexity introduced on purpose to handle extra cases while data conversion

        if (!string.IsNullOrEmpty(movieShort.Year))
        {
            // Assumption is to get the first 4 digits from year row, because it could be years range etc.
            var yearPattern = new Regex("^\\d{4}", RegexOptions.None, TimeSpan.FromMilliseconds(100));
            var match = yearPattern.Match(movieShort.Year);
            if (match.Success)
            {
                this.Year = int.Parse(match.Groups[0].Value);
            }
            else
            {
                throw new FormatException("Unknown year format");
            }
        }
    }
}