﻿using OBMDbClient.Models.OmdbApi;

namespace OBMDbClient.Models.Dto;

public record MovieDetailDto : MovieShortDto
{
    public string? TimeLength { get; set; }
    public string? Genre { get; set; }
    public string? Writer { get; set; }
    public string? Actors { get; set; }
    public string? Language { get; set; }
    public string? Country { get; set; }
    public string? PosterUrl { get; set; }

    public MovieDetailDto(MovieDetail movieDetail) : base(movieDetail)
    {
        this.TimeLength = movieDetail.Runtime;
        this.Genre = movieDetail.Genre;
        this.Writer = movieDetail.Writer;
        this.Actors = movieDetail.Actors;
        this.Language = movieDetail.Language;
        this.Country = movieDetail.Country;
        this.PosterUrl = movieDetail.Poster;
    }
}
