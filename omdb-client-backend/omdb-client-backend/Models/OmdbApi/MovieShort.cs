﻿namespace OBMDbClient.Models.OmdbApi;

public record MovieShort
{
    public required string ImdbID { get; set; }
    public required string Title { get; set; }
    public required string Year { get; set; }
}