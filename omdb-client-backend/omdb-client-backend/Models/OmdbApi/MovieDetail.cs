﻿namespace OBMDbClient.Models.OmdbApi;

public record MovieDetail : MovieShort
{
    public string? Runtime { get; set; }
    public string? Genre { get; set; }
    public string? Writer { get; set; }
    public string? Actors { get; set; }
    public string? Language { get; set; }
    public string? Country { get; set; }
    public string? Poster { get; set; }
}