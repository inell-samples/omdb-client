﻿namespace OBMDbClient.Models.OmdbApi;

public record SearchResponse
{
    public IEnumerable<MovieShort>? Search { get; set; }
    public int? TotalResults { get; set; }
    public bool Response { get; set; }
    public string? Error { get; set; }
}
