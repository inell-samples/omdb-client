﻿namespace OBMDbClient.Models.OmdbApi;

public record MovieResponse : MovieDetail
{
    public bool Response { get; set; }
    public string? Error { get; set; }
}
