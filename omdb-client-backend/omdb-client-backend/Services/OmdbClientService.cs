﻿using OBMDbClient.Models;
using OBMDbClient.Models.Dto;

namespace OBMDbClient.Services;

public interface IOmdbClientService
{
    /// <summary>
    /// Retrieves movie details by <paramref name="imdbID"/>
    /// </summary>
    /// <param name="imdbID"></param>
    /// <returns></returns>
    Task<MovieDetailResultResponse> GetMovieDetails(string imdbID);

    /// <summary>
    /// Retrieves search results by <paramref name="title"/>
    /// </summary>
    /// <param name="title"></param>
    /// <returns></returns>
    Task<SearchResultResponse> Search(string title);
}

public class OmdbClientService : IOmdbClientService
{
    private readonly IHttpOmdbApiService _httpClient;

    public OmdbClientService(IHttpOmdbApiService httpClient)
    {
        _httpClient = httpClient;
    }

    public async Task<MovieDetailResultResponse> GetMovieDetails(string imdbID)
    {
        ArgumentException.ThrowIfNullOrEmpty(imdbID);

        var result = await _httpClient.GetMovieDetails(imdbID);
        if (result == null)
        {
            return new MovieDetailResultResponse
            {
                Success = false,
                ErrorMessage = "Bad gateway"
            };
        }

        var response = new MovieDetailResultResponse();
        try
        {
            response.Data = result.Response ? new MovieDetailDto(result) : null;
        }
        catch (Exception)
        {
            response.Success = false;
            response.ErrorMessage = "Problem when parsing data";
        }

        return response;
    }

    public async Task<SearchResultResponse> Search(string title)
    {
        ArgumentException.ThrowIfNullOrEmpty(title);

        var result = await _httpClient.Search(title);
        if (result == null)
        {
            return new SearchResultResponse
            {
                Success = false,
                ErrorMessage = "Bad gateway"
            };
        }

        var response = new SearchResultResponse();
        try
        {
            // Materializing by ToArray() is required to catch internal error
            response.Data = result.Search?.Select(x => new MovieShortDto(x)).ToArray() ?? Array.Empty<MovieShortDto>();
            response.Total = result.TotalResults ?? 0;
        }
        catch (Exception)
        {
            response.Success = false;
            response.ErrorMessage = "Problem when parsing data";
        }
     
        return response;
    }
}
