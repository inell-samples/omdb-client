﻿using Microsoft.Extensions.Options;
using OBMDbClient.Configuration;
using OBMDbClient.Models.OmdbApi;
using OBMDbClient.Platform;
using System.Collections.Specialized;
using System.Text.Json;
using System.Web;

namespace OBMDbClient.Services;

public interface IHttpOmdbApiService
{
    /// <summary>
    /// Retrieves movie details by <paramref name="imdbID"/>
    /// </summary>
    /// <param name="imdbID"></param>
    /// <returns></returns>
    Task<MovieResponse?> GetMovieDetails(string imdbID);

    /// <summary>
    /// Retrieves search results by <paramref name="title"/>
    /// </summary>
    /// <param name="title"></param>
    /// <returns></returns>
    Task<SearchResponse?> Search(string title);
}

public class HttpOmdbApiService : IHttpOmdbApiService
{
    private readonly HttpClient _client;
    private readonly IOptions<ServicesOptions> _options;
    private readonly ILogger<HttpOmdbApiService> _logger;

    public HttpOmdbApiService(HttpClient client, IOptions<ServicesOptions> options, ILogger<HttpOmdbApiService> logger)
    {
        _client = client;
        _options = options;
        _logger = logger;

        _client.BaseAddress = options.Value.OmdbApi;
    }

    public Task<MovieResponse?> GetMovieDetails(string imdbID)
    {
        ArgumentException.ThrowIfNullOrEmpty(imdbID);

        var queryString = HttpUtility.ParseQueryString(string.Empty);
        queryString.Add("i", imdbID);

        return this.SendRequest<MovieResponse>(queryString);
    }

    public Task<SearchResponse?> Search(string title)
    {
        ArgumentException.ThrowIfNullOrEmpty(title);

        var queryString = HttpUtility.ParseQueryString(string.Empty);
        queryString.Add("s", title);

        return this.SendRequest<SearchResponse>(queryString);
    }

    protected async Task<T?> SendRequest<T>(NameValueCollection queryString)
    {
        queryString.Add("apikey", _options.Value.OmdbApiKey);
        queryString.Add("r", "json");

        var options = new JsonSerializerOptions(JsonSerializerDefaults.Web);
        options.Converters.Add(new JsonBooleanConverter());

        try
        {
            return await _client.GetFromJsonAsync<T>("?" + queryString.ToString(), options);
        }
        catch (Exception ex)
        {
            _logger.LogWarning(ex, ex.Message);
            return default;
        }
    }
}
