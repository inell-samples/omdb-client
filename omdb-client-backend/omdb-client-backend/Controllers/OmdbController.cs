﻿using Microsoft.AspNetCore.Mvc;
using OBMDbClient.Models;
using OBMDbClient.Services;
using System.ComponentModel.DataAnnotations;

namespace omdb_client_backend.Controllers;

[Route("api/[controller]")]
[ApiController]
public class OmdbController : ControllerBase
{
    private readonly IOmdbClientService _clientService;

    public OmdbController(IOmdbClientService clientService)
    {
        _clientService = clientService;
    }

    [HttpGet("[action]")]
    public Task<SearchResultResponse> Search([Required] string title)
    {
        return _clientService.Search(title);        
    }

    [HttpGet("[action]")]
    public Task<MovieDetailResultResponse> Details([Required] string imdbID)
    {
        return _clientService.GetMovieDetails(imdbID);
    }
}
