# omdb-client

This is example of .NET Core **backend-service** using [omdbapi.com](https://www.omdbapi.com/) for retrieving movie data and React **frontend-service** to visualize the data.

## Functional requirements 

- Search movies by title;
- Visualize search results or "Not found" placeholder;
- Persist 5 latest search queries;
- Loader placeholder;
- Show extended movie information for particular movie;

## Launching

1) Start backend 

   ```bash
   > dotnet run
   ```

2) Start frontend

   ```bash
   > npm run dev
   ```

3) Follow the URL provided in the output by `Vite`

## Storing credentials

OMDb API requires a key provided for each request. This key is being managed as application secret stored on a developer machine in `%APPDATA%\Microsoft\UserSecrets\<user_secrets_id>\secrets.json`. [More details](https://learn.microsoft.com/en-us/aspnet/core/security/app-secrets).

To add the secret, run command

```bash
> dotnet user-secrets set "Services:OmdbApiKey" "Yout_API_key"
```

Or for testing purposes you can just put the key into `appConfig.json` file.

